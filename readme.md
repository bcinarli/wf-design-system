# WF Design System
Design system implementation is a demo setup of creating separate and re-usable (almost) independent packages. In theory, when the packages build and published to public or private npm registry, they can be installable individually. Though, according to the task, only the `Button` and `Tooltip` components and `fetch` helper is available.

## Development Setup
Component development set up to use React with Typescript. Local env doesn't support using plain `js/jsx` for development. For testing the components, Jest and Testing Library are available. Code building is provided by Vitejs. All code and styles expected to run in modern browsers.

## Development
After cloning to repository and installing to dependencies, to run the local environment run
```
// if you are using npm
$ npm run dev 

// if you are using yarn
$ yarn dev
```
Vitejs will start the local environment almost immediately. After you can navigate to http://localhost:3000

There is a dummy local app that renders the components for visual testing.

CSS modules is used for styling for the sake of simplicity. If needed, all components can easily convertable to use styled-components.

## Testing
Jest set up to run test files in jsdom environment.
```
// if you are using npm
$ npm run test

// if you are using yarn
$ yarn test
```
commands run the unit tests.

## Requirement Notes
* If there is no `url` definition in the buttons, there will be no network request attempts done on click. Button will fail/ignore click silently.
* If there is a network error/timeout, re-clicking to the button with _error state_ restart the request.
* Web's native Fetch API and AbortController is used for requests and cancellations.
* There is a default timeout about 300 seconds when an explicit timeout in milliseconds is not defined.

### Notes on Storybook
Storybook is not supporting React 18 yet, I tried a couple different "hacks", but could not be able to make it work, mostly hitting to _peer dependency_ errors and some incompatibilities.
