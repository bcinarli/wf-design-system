type FetchReturnType = {
  status: number;
  statusText: string;
  ok: boolean;
  data: null | object;
};

type ErrorType = FetchReturnType | Response;

const DEFAULT_TIMEOUT = 300000; // 300 seconds

const fetchURL = async (
  url: string,
  timeout: number = DEFAULT_TIMEOUT,
  controller?: AbortController
): Promise<FetchReturnType> => {
  const abortController = controller ?? new AbortController();
  const fetchTimeout = setTimeout(() => abortController.abort(), timeout);
  let error: ErrorType = {
    status: 418,
    statusText: "I'm a teapot",
    ok: false,
    data: null
  };

  try {
    const response = await fetch(url, { signal: abortController.signal });
    clearTimeout(fetchTimeout);

    if (!response.ok) {
      error = response;

      throw new Error(`Http error`);
    }

    const data = await response.json();

    return {
      status: response.status,
      statusText: response.statusText,
      ok: true,
      data
    };
  } catch (e) {
    return {
      status: error.status,
      statusText: error.statusText,
      ok: false,
      data: null
    };
  }
};

export default fetchURL;
