import { render, screen } from '@testing-library/react';
import Tooltip from './tooltip';

test('Renders a tooltip', () => {
  const ToolTipText = 'Ignites the rocket';
  render(<Tooltip id={'tooltip'} text={ToolTipText} type={'default'} />);
  expect(screen.getByText(ToolTipText)).toBeInTheDocument();
});

test('Does not render a tooltip if text is empty', () => {
  const ToolTipText = '';
  render(<Tooltip id={'tooltip'} text={ToolTipText} type={'default'} />);
  expect(screen.queryByTestId('tooltip')).not.toBeInTheDocument();
});
