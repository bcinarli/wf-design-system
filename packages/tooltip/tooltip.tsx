import classNames from 'classnames';
import classes from './tooltip.module.css';

type TooltipProps = {
  id: string;
  text: string;
  type: 'default' | 'error' | 'progress';
};

const Tooltip = ({ text, type, id }: TooltipProps): JSX.Element => {
  return text ? (
    <div
      id={id}
      data-testid={id}
      aria-hidden={false}
      className={classNames(
        classes['wfds-tooltip'],
        classes[`wfds-tooltip-${type}`]
      )}
    >
      {text}
    </div>
  ) : (
    <></>
  );
};

export default Tooltip;
