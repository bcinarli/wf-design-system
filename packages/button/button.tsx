import { useState, useRef, ReactNode, MouseEvent } from 'react';
import classNames from 'classnames';

import fetchURL from '@wf-design-system/helpers/fetch';
import Tooltip from '@wf-design-system/tooltip/tooltip';

import classes from './button.module.css';

type ButtonProps = {
  name: string;
  children?: ReactNode;
  url?: string;
  timeout?: number;
  disabled?: boolean;
  initialState?: 'default' | 'error' | 'progress';
  toolTip?: {
    default?: string;
    progress?: string;
    error?: string;
  };
};

type ButtonStates = 'default' | 'error' | 'progress';

const Button = ({
  name,
  children,
  url,
  timeout,
  toolTip,
  disabled = false,
  initialState = 'default'
}: ButtonProps): JSX.Element => {
  const [state, setState] = useState<ButtonStates>(initialState);
  const controller = useRef(new AbortController());

  const handleClick = async (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    if (!url) {
      return;
    }

    // abort the button action in second click
    if (state === 'progress') {
      controller.current.abort();
      setState('error');
      // reset the abort controller
      controller.current = new AbortController();
      return;
    }

    setState('progress');

    const response = await fetchURL(url as string, timeout, controller.current);

    // when we receive an error from response
    // either because of timeout, or fetch error
    if (!response.ok) {
      setState('error');
      // reset controller to be able to restart to request
      controller.current = new AbortController();
    } else {
      setState('default');
    }
  };

  return (
    <button
      className={classNames(
        classes['wfds-button'],
        classes[`wfds-button-${state}`],
        {
          'wfds-tooltip-trigger': toolTip,
          'wfds-tooltip-sticky': state === 'error'
        }
      )}
      disabled={disabled}
      aria-describedby={`${name}-hint`}
      onClick={handleClick}
    >
      {children}
      {toolTip && !disabled && (
        <Tooltip
          id={`${name}-hint`}
          text={toolTip?.[state] ?? ''}
          type={state}
        />
      )}
    </button>
  );
};

export default Button;
