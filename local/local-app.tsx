import Button from "../packages/button/button";

const tooltip = { default: "Ignites the fuel", progress: "Cancel Launch", error: "Ignition Error" }

const LocalApp = () => {
    return (<>
        <div>
            <h2>Default Usage</h2>
            <Button
                name="rocket-launch"
                toolTip={tooltip}
                url={"https://httpbin.org/get"}
            >Launch Rocket</Button>
        </div>
        <div>
            <h2>Default Usage with 2 seconds delay in request</h2>
            <Button
              name="rocket-launch-delay"
              toolTip={tooltip}
              url={"https://httpbin.org/delay/2"}
            >Launch Rocket</Button>
        </div>
        <div>
            <h2>Request timeout</h2>
            <Button
                name="rocket-launch-timeout"
                toolTip={tooltip}
                url={"https://httpbin.org/delay/5"}
                timeout={2000}
            >Launch Rocket</Button>
        </div>
        <div>
            <h2>Disabled (doesn't make a request)</h2>
            <Button name="rocket-launch-disabled" disabled>Launch Rocket</Button>
        </div>
        <div>
            <h2>Initial State Progress (doesn't make a request)</h2>
            <Button name="rocket-launch-progress" initialState="progress" toolTip={tooltip}>Launch Rocket</Button>
        </div>
        <div>
            <h2>Initial State Error (doesn't make a request)</h2>
            <Button name="rocket-launch-error" initialState="error" toolTip={tooltip}>Launch Rocket</Button>
        </div>
    </>)
}

export default LocalApp
