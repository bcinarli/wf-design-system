import { StrictMode } from "react";
import ReactDOM from "react-dom/client";
import LocalApp from "./local-app";

ReactDOM.createRoot(document.getElementById('root')!).render(
    <StrictMode>
        <LocalApp/>
    </StrictMode>
)
